import { createRouter, createWebHistory } from 'vue-router';
import Home from '/src/views/Home.vue';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('%/Login.vue'),
    },
    {
        path: '/signup',
        name: 'Sign Up',
        component: () => import('%/SignUp.vue'),
    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => import('%/Settings.vue'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
