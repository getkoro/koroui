import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import WindiCSS from 'vite-plugin-windicss';
import Components from 'vite-plugin-components';
import ViteIcons, { ViteIconsResolver } from 'vite-plugin-icons';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        WindiCSS(),
        Components({
            customComponentResolvers: ViteIconsResolver(),
            globalComponentsDeclaration: true
        }),
        ViteIcons(),
    ],

    resolve: {
        alias: [
            {find: '@', replacement: '/src'},
            {find: '%', replacement: '/src/views'},
            {find: '§', replacement: '/src/assets/img'},
        ],
    },
});
