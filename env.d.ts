interface ImportMetaEnv {
    VITE_WEATHER_LONGITUDE: number;
    VITE_WEATHER_LATITUDE: number;
    VITE_WEATHER_TOKEN: string;
}
