// tailwind.config.ts
import {defineConfig} from 'windicss/helpers';
import formsPlugin from 'windicss/plugin/forms';

export default defineConfig({
    darkMode: 'class',
    theme: {
        extend: {
            colors: {
                teal: {
                    100: '#096',
                },
            },
            gridTemplateColumns: {
                'setting': '500px 100px',
            },
        },
    },
    shortcuts: {
        'text': 'text-base font-medium text-gray-800 dark:text-gray-200',
        'text-10': 'text-base font-medium text-gray-600 dark:text-gray-300',
        'text-20': 'text-base font-medium text-gray-400 dark:text-gray-400',
        'text-30': 'text-base font-medium text-gray-300 dark:text-gray-500',
        'bg': 'bg-white dark:bg-gray-800'
    },
    plugins: [
        formsPlugin,
        require('@windicss/plugin-interaction-variants'),
    ],
});
